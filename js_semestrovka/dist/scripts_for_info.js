class FilmApp 
    {
    constructor() 
        {
        this.get_user_name = document.getElementById("get_user_name");
        this.user_name = document.getElementById("user_name");
        this.mains_page = document.getElementById("mains_page");
        this.saved_films = document.getElementById("saved_films");
        this.resemble_films = document.getElementById("resemble_films");
        this.next_films = document.getElementById("next_films");
        this.div_for_contents_name = document.getElementById("div_for_contents_name");
        this.div_for_contents_obzor = document.getElementById("div_for_contents_obzor");
        this.div_for_contents_raiting = document.getElementById("div_for_contents_raiting");
        this.href_film = document.getElementById("href_film");
        this.img = document.getElementById("img");
        this.loader = document.getElementById("loader");
        this._filmes = "";
        this._movID = "";
        this._filmes_id = 0;
        this._imdb_o = "";
        this._year_films = 0;
        this._description_film = "";
        this._poster_url = "";
        this._hrefUrl = "";
    }
    get movID() {
        return this._movID;
    }
    set movID(value) {
        this._movID = value;
    }
    get filmes() {
        return this._filmes;
    }
    set filmes(value) {
        this._filmes = value;
    }
    get filmes_id() {
        return this._filmes_id;
    }
    set filmes_id(value) {
        this._filmes_id = value;
    }
    get imdb_o() {
        return this._imdb_o;
    }
    set imdb_o(value) {
        this._imdb_o = value;
    }
    get year_films() {
        return this._year_films;
    }
    set year_films(value) {
        this._year_films = value;
    }
    get description_film() {
        return this._description_film;
    }
    set description_film(value) {
        this._description_film = value;
    }
    get poster_url() {
        return this._poster_url;
    }
    set poster_url(value) {
        this._poster_url = value;
    }
    get hrefUrl() {
        return this._hrefUrl;
    }
    set hrefUrl(value) {
        this._hrefUrl = value;
    }
};
export { FilmApp };