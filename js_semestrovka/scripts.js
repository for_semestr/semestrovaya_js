import { check_user_name, createAndAppendDiv, dis_false } from "./dist/scripts_for_button.js"
import { getRandomMovie, getSimilarOrRandomMovie, displayMovieDetails, eror_for_content } from "./dist/scripts_for_kinopoisk.js"; 
import { FilmApp } from "./dist/scripts_for_info.js";
const FA = new FilmApp();
get_user_name.onclick = async function()
    {
	check_user_name(user_name, welcome_page, mains_page);
    FA.loader.style["display"] = "block";
    dis_false(saved_films);
    try 
    {
        FA.movID = await getRandomMovie();
        displayMovieDetails(FA.movID);
    } 
    catch (err) 
    {
        alert('Ошибка при получении информации о фильме: перезагрузите сервис или обратитесь по номер 89951640595');
        console.error('Ошибка при получении информации о фильме:', err.message || err);
    } 
    finally 
    {
        div_for_contents_name.innerHTML = FA.filmes + " " + FA.year_films;
        div_for_contents_obzor.innerHTML = FA.description_film;
        div_for_contents_raiting.innerHTML = FA.imdb_o;
        img.src = FA.poster_url;
        FA.hrefUrl = 'https://www.kinopoisk.ru/film/' + FA.filmes_id + '/';
        href_film.innerHTML = FA.hrefUrl;
        FA.loader.style["display"] = "none";
    }
};
document.getElementById('myForm_1').addEventListener('submit', function(event) 
    {
    event.preventDefault();
    check_user_name(user_name, welcome_page, mains_page);
});
next_films.onclick = async function() 
    {
    FA.loader.style["display"] = "block";
    dis_false(saved_films);
    try {
        FA.movID = await getRandomMovie();
        displayMovieDetails(FA.movID);
    } 
    catch (err) {
        alert('Ошибка при получении информации о фильме: перезагрузите сервис или обратитесь по номер 89951640595');
        console.error('Ошибка при получении информации о фильме:', err.message || err);
    } 
    finally {
        div_for_contents_name.innerHTML = FA.filmes + " " + FA.year_films;
        div_for_contents_obzor.innerHTML = FA.description_film;
        div_for_contents_raiting.innerHTML = FA.imdb_o;
        img.src = FA.poster_url;
        FA.hrefUrl = 'https://www.kinopoisk.ru/film/' + FA.filmes_id + '/';
        href_film.innerHTML = FA.hrefUrl;
        FA.loader.style["display"] = "none";
    }
};
resemble_films.onclick = async function() 
{
    dis_false(saved_films);
    FA.loader.style["display"] = "block";
    try {
        FA.movID = await getSimilarOrRandomMovie(FA.filmes_id);
        displayMovieDetails(FA.movID);
    } 
    catch (err) {
        console.error('Ошибка при получении информации о фильме:', err.message || err);
    } 
    finally {
        div_for_contents_name.innerHTML = FA.filmes + " " + FA.year_films;
        div_for_contents_obzor.innerHTML = FA.description_film;
        div_for_contents_raiting.innerHTML = FA.imdb_o;
        img.src = FA.poster_url;
        FA.hrefUrl = 'https://www.kinopoisk.ru/film/' + FA.filmes_id + '/';
        href_film.innerHTML = FA.hrefUrl;
        FA.loader.style["display"] = "none";
    }
};
saved_films.onclick = async function()
    {
	createAndAppendDiv(div_for_contents_name.innerHTML, img.src, href_film.innerHTML);
};
document.addEventListener('keydown', function(event) 
    {
    if (FA.loader.style["display"] === "none" && event.keyCode === 39) {
        document.getElementById("next_films").click();
    }
});
document.addEventListener('keydown', function(event) 
    {
    if (FA.loader.style["display"] === "none" && event.keyCode === 37) {
        document.getElementById("resemble_films").click();
    }
});
document.addEventListener('keydown', function(event) 
    {
    if (FA.loader.style["display"] === "none" && (event.key === 's' || event.key === 'S')) {
        document.getElementById("saved_films").click();
    }
});