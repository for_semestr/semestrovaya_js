function check_user_name(user_name, welcome_page, mains_page)
	{
	if (user_name.value.trim() != '') {
		welcome_page.style["display"] = "none";
		mains_page.style["display"] = "block";
		document.getElementById("hello_head").innerHTML = "Привет " + user_name.value;
	}
};

function dis_false(button_saved)
	{
	saved_films.disabled = false;
};
function createAndAppendDiv(filmes, poster_url, hrefUrl) 
	{
	let existingDivCount = document.getElementById('div_page_saved').querySelectorAll('.saved_films').length;
	if (existingDivCount != 10){
		let container = document.getElementById('div_page_saved');
		let newDiv = document.createElement('div');
		newDiv.classList.add('saved_films');
		newDiv.style.width = '20%';
		newDiv.style.height = '50%';
		newDiv.style.margin = '0px';
		newDiv.style.display = 'block';
		newDiv.style.float = 'left';
		newDiv.style.position = 'relative';
		newDiv.style.color = '#E6E2DD';
		newDiv.style.textAlign = 'center';
		let linkElement = document.createElement('a');
		linkElement.href = hrefUrl; 
		linkElement.textContent = filmes;
		linkElement.style.position = "absolute"; 
		linkElement.style.display = "block";
		linkElement.style.top = "80%";
		let imgElement = document.createElement('img');
		imgElement.src = poster_url;
		imgElement.style["object-fit"] = "fill"; 
		imgElement.style.width = "100%"; 
		imgElement.style.height = "80%";
		imgElement.style.position = "absolute"; 
		imgElement.style.display = "block";
		let deleteButton = document.createElement('button');
		deleteButton.textContent = 'X';
		deleteButton.style.float = 'right';
		deleteButton.style.position = 'relative';
		deleteButton.style.backgroundColor = "red";
		deleteButton.style["z-index"] = '33';
		deleteButton.onclick = function() 
		{
			this.parentElement.remove();
		};
		newDiv.appendChild(imgElement);
		newDiv.appendChild(linkElement);
		newDiv.appendChild(deleteButton);
		container.appendChild(newDiv);
		saved_films.disabled = true;
		alert("Вы сохранили фильм");
	}
	else {
		alert("Вы сохранили достаточно, удалите то, что вам не интересно")
	}
}
export {check_user_name, createAndAppendDiv, dis_false};