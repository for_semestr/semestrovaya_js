import { FilmApp } from "./scripts_for_info.js";
const FA = new FilmApp();
const apiKeys = [
    '7Y3TCA6-96M46W0-NP9XT5D-CPP7DZG', 
    'C2DJJP8-5ZNMKTA-NNN6RXD-5T7J0M3', 
    'XNWJZAG-YJVMC2K-N15J9HM-WMGS2XX', 
    'PSGRMT3-YEK44HM-NF15D0B-2RHM3B9'
  ];
function getRandomApiKey() {
    const randomIndex = Math.floor(Math.random() * apiKeys.length);
    return apiKeys[randomIndex];
};
const options = {
    method: 'GET',
    headers: {
        accept: 'application/json',
        'X-API-KEY': getRandomApiKey()
    }
};
let requestCount = 0;
function eror_for_content(err, typ)
    {
    if (err == null && typ == "img"){
        err = "./dist/ERROR.png";
        return err;
    }
    else if ((err == null || err == "") && typ == "obzor"){
        err = "Тут должно быть описание фильма, но нет денег на платный API";
        return err;
    }
    else if (err == "0" && typ == "rating"){
        err = "Нет денег на API";
        return err;
    }
    else if (err == null && typ == "year"){
        err = "Нет денег на API";
        return err;
    }
    return err;
};
async function getRandomMovie() 
    {
    while (true) {
        requestCount++;
        const randomMovieResponse = await fetch('https://api.kinopoisk.dev/v1.4/movie/random', options);
        const randomMovieData = await randomMovieResponse.json();
        if (randomMovieData.name) {
            return randomMovieData.id;
        }
    }
};
async function getSimilarOrRandomMovie(filmes_id) 
    {
    const randomMovieResponse = await fetch(`https://api.kinopoisk.dev/v1.4/movie/${filmes_id}`, options);
    const randomMovieData = await randomMovieResponse.json();
    if (randomMovieData.similarMovies && randomMovieData.similarMovies.length > 0) {
        return randomMovieData.similarMovies[0].id;
    } 
    else {
        return await getRandomMovie(options);
    }
};
async function displayMovieDetails(movID) 
    {
    const movieInfoResponse = await fetch(`https://api.kinopoisk.dev/v1.4/movie/${movID}`, options);
    const movieInfoData = await movieInfoResponse.json();
    FA.filmes = movieInfoData.name;
    FA.year_films = eror_for_content(movieInfoData.year, "year");
    FA.imdb_o = eror_for_content(movieInfoData.rating.imdb, "rating");
    FA.description_film = eror_for_content(movieInfoData.description, "obzor");
    FA.poster_url = eror_for_content(movieInfoData.poster.previewUrl, "img");
    FA.filmes_id = movID;
    if (movieInfoResponse.status != 200)
    {
        alert("Ошибка при получении информации о фильме: перезагрузите сервис или обратитесь по номер 89951640595");
    };
    div_for_contents_name.innerHTML = `${FA.filmes} (${FA.year_films})`;
    div_for_contents_obzor.innerHTML = "Обзор:<br>" + FA.description_film; 
    div_for_contents_raiting.innerHTML = "Рейтинг: " + FA.imdb_o; 
    img.src = FA.poster_url;
    FA.hrefUrl = 'https://www.kinopoisk.ru/film/' + FA.filmes_id + '/'; 
    href_film.innerHTML = FA.hrefUrl;
    FA.loader.style["display"] = "none";
}
export { getRandomMovie, getSimilarOrRandomMovie, displayMovieDetails, eror_for_content };